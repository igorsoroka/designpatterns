package complexShape;

public class Rectangle implements Shape {
    
    private String color;
    
    public Rectangle(String color) {
        this.color = color;
    }

    @Override
    public void draw() {
        System.out.println("The " + this.getColor() + " rectangle is created");
    }

    public String getColor() {
        return color;
    }
    
}
