package complexShape;

import java.util.ArrayList;

public class ComplexShape implements Shape {

    private ArrayList<Shape> shapes;

    public ComplexShape() {
        shapes = new ArrayList<>();
    }

    @Override
    public void draw() {
        for (Shape s : shapes) {
           s.draw();
        }
    }

    public void addShape(Shape sh) {
        shapes.add(sh);
    }
}
