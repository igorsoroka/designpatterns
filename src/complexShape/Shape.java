package complexShape;

public interface Shape {

    public void draw();
}
