package complexShape;

public class MainClass {

    public static void main(String[] args) {
        
        Shape circle1 = new Circle("white", 5);
        Shape circle2 = new Circle("black", 3);
        
        Shape rctngl1 = new Rectangle("pink");
        Shape rctngl2 = new Rectangle("green");
        
        ComplexShape cmplx = new ComplexShape();
        
        cmplx.addShape(circle1);
        cmplx.addShape(rctngl1);
        cmplx.addShape(circle2);
        cmplx.addShape(rctngl2);
        cmplx.draw();
    }
}
