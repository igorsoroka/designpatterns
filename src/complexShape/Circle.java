package complexShape;

public class Circle implements Shape {

    private int diameter;
    private String color;

    public Circle(String color, int d) {
        this.color = color;
        diameter = d;
    }

    public void draw() {
        System.out.println("The " + this.getColor() 
                            + " circle of diameter " + this.getDiameter()
                            + " is created");
    }

    public int getDiameter() {
        return diameter;
    }

    public String getColor() {
        return color;
    }
    
}
