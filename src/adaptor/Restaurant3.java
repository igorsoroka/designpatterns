package adaptor;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

public class Restaurant3 {
      private Enumeration menuEnum;
      
      public Enumeration<String> enumMenu() {
          
          String[] menuArray = new String[]{
            "Miso Soup /6\u20AC / Japan", 
            "Mexican soup / 10\u20AC / Very HOT", 
            "Borsch / 11\u20AC / Russian classics!", 
            "Irish homemade soup / 16.45\u20AC / Guinness inside",
            "Cheese soup / 7\u20AC / The cheapest in town"};
          Vector<String> v = new Vector<>();
          v.addAll(Arrays.asList(menuArray));
          menuEnum = v.elements();
          return menuEnum;
      }
      
      public EnumerationIteration createIterator() {
        return new EnumerationIteration(enumMenu());
    }
}