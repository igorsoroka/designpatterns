package adaptor;

public class Restaurant1 {
    private String[] menuArray = new String[]{
            "BeefBurger /15.80\u20AC / Famous and tasty burger. El classico!/", 
            "Pasta Bolongse / 12.50\u20AC / From Italy with love.", 
            "Cesare salad / 10.20\u20AC / The best dishes for those who on diet.", 
            "Fish and Chips / 16.45\u20AC / The best thing from UK. Recommended with Guinness Stout.",
            "Cheese soup / 8\u20AC / This mild cheese cream soup with croutons will conquer your heart!"};

    public Restaurant1Iterator createIterator() {
        return new Restaurant1Iterator(menuArray);
    }
}
