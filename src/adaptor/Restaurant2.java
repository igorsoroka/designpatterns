package adaptor;

import java.util.ArrayList;

public class Restaurant2 {

    private ArrayList<String> menuArrayList = new ArrayList<>();

    public ArrayList<String> setMenuArrayList(ArrayList<String> menuArrayList) {
        this.menuArrayList = menuArrayList;
        menuArrayList.add("Beef Kebab/ 7\u20AC / From Turkey's best cafes.");
        menuArrayList.add("Falafel Pita/ 6\u20AC / Vegetarian best thing from Israel.");
        menuArrayList.add("Pizza 4 cheeses/ 12\u20AC / Absolute hit of all times!");
        menuArrayList.add("Pizza Italian Melody/ 14\u20AC / Italian salami with parmezan. MMMM!");
        return menuArrayList;
    }
    
    
    
    public Restaurant2Iterator createIterator() {
        return new Restaurant2Iterator(this.setMenuArrayList(menuArrayList));
    }
}
