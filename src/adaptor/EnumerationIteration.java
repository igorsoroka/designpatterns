package adaptor;

import java.util.Enumeration;

public class EnumerationIteration implements Iterator {

    Enumeration e;    
    
    EnumerationIteration(Enumeration<String> enumMenu) {
        e = enumMenu;
    }
    
    public boolean hasNext() {
        return e.hasMoreElements();
    }

    public Object next() {
        return e.nextElement();
    }
}
