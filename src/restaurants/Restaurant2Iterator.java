package restaurants;

import java.util.ArrayList;

public class Restaurant2Iterator implements Iterator {
    int index;
    ArrayList<String> menuArrayList;

    public Restaurant2Iterator(ArrayList<String> menuArrayList) {
        index = 0;
        this.menuArrayList = menuArrayList;
    }
    
    @Override
    public Object next() {
        return menuArrayList.get(index++);
    }

    @Override
    public boolean hasNext() {
        if (menuArrayList.size() == index)
            return false;
        else
            return true;
    }

}
