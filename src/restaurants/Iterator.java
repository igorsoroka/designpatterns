package restaurants;

public abstract interface Iterator {

    public Object next();

    public boolean hasNext();
}
