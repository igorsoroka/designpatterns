package restaurants;

public class Restaurant1Iterator implements Iterator {
    int index;
    private String[] menuArray;

    public Restaurant1Iterator(String[] menuArray) {
        index = 0;
        this.menuArray = menuArray;
    }
    
    @Override
    public Object next() {
        return menuArray[index++];
    }

    @Override
    public boolean hasNext() {
        if (menuArray.length == index)    
            return false;
        else
            return true;
        }
}
