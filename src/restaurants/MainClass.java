package restaurants;

public class MainClass {

    public static void main(String[] args) {
        Restaurant1 res1 = new Restaurant1();
        Restaurant2 res2 = new Restaurant2();
        System.out.println("***MENU OF RESTAURANT 1***");
        for (Iterator it1 = res1.createIterator(); it1.hasNext();) {
            String item = (String)it1.next();
            System.out.println(item);
        }
        System.out.println();
        System.out.println("***MENU OF RESTAURANT 2***");
        for (Iterator it2 = res2.createIterator(); it2.hasNext();) {
            String item = (String)it2.next();
            System.out.println(item);
        }
    }
}
