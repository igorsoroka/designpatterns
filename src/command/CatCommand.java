package command;

public class CatCommand implements Command {

    private Cat cat;

    public CatCommand(Cat cat) {
        this.cat = cat;
    }
    @Override
    public void execute() {
        cat.activateDeactivate();
    }
}
