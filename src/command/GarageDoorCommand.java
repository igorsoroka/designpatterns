package command;

public class GarageDoorCommand implements Command {

    private GarageDoor gdoor;

    public GarageDoorCommand(GarageDoor gdoor) {
        this.gdoor = gdoor;
    }

    public void execute() {
        gdoor.openClose();
    }
}
