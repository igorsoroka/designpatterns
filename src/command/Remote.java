package command;

public class Remote {

    private Command[] slots;

    public Remote() {
        slots = new Command[3];
    }

    public void setCommand(int slot, Command command) {
        slots[slot] = command;
    }

    public void buttonWasPressed(int slot) {
        slots[slot].execute();
    }
}
