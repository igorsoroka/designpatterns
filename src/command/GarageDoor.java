package command;

public class GarageDoor {

    private boolean doorOpened = false;

    public void openClose() {
        if (doorOpened == false) {
            System.out.println("The door was opened");
            doorOpened = true;
        }
        else {
            System.out.println("The door was closed");
            doorOpened = false;
        }
    }
}
