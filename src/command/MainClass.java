package command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainClass {

    public static void main(String[] args) throws IOException {
        //Creating objects 
        Remote rc = new Remote();
        Light light = new Light();
        GarageDoor gd = new GarageDoor();
        Cat cat = new Cat();
        LightOnCommand lightCom = new LightOnCommand(light);
        GarageDoorCommand gdoorCom = new GarageDoorCommand(gd);
        CatCommand catCom = new CatCommand(cat);
        //Assigning commands to slots
        rc.setCommand(0, lightCom);
        rc.setCommand(1, gdoorCom);
        rc.setCommand(2, catCom);
        //Creating menu and input/output
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> remoteControl = new ArrayList<>();
        remoteControl.add("*****REMOTE CONTROL OF SMART HOME BY IGOR SOROKA****");
        remoteControl.add("Button 1 - Turn on/off the light");
        remoteControl.add("Button 2 - Open/close the garage door");
        remoteControl.add("Button 3 - Turn on/off the CAT");
        remoteControl.add("Button 0 - STOP! *THROWING AWAY*");
        //remote control menu
        int press = -1;
        while (press != 0) {
            remoteControl.stream().forEach((s) -> {
                System.out.println(s);
            });
            try {
              press = Integer.parseInt(br.readLine());
              switch(press) {
                  case 1:
                      rc.buttonWasPressed(0);
                      break;
                  case 2:
                      rc.buttonWasPressed(1);
                      break;
                  case 3:
                      rc.buttonWasPressed(2);
                      break;
                  default:
                      System.out.println("Please enter 0, 1, 2, 3 only");
                      break;
              }
            }
            catch (Exception e) {
                System.out.println("Please enter 0, 1, 2, 3 only");
            }
            System.out.println();
        }  
    }
}
