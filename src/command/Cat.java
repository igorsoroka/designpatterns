package command;

public class Cat {

    private boolean catActive = false;

    public void activateDeactivate() {
        if (catActive == false) {
            System.out.println("The cat started to run around!");
            catActive = true;
        }
        else {
            System.out.println("The cat freezed!");
            catActive = false;
        }
    }
}
