package command;

public class Light {

    private boolean lightOn = false;

    public void onOff() {
       if (lightOn == false) {
           System.out.println("The light is on");
           lightOn = true;
       }
       else {
           System.out.println("The light is off");
           lightOn = false;
       }
    }
}
