package mobiles;

public interface Observer {

    public void update(Subject s);
}
