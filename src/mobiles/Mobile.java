package mobiles;

public class Mobile implements Observer {

    private String mobileName;

    public Mobile(String mobileName) {
        this.mobileName = mobileName;
    }
 
    public String getMobileName() {
        return mobileName;
    }
    
    

    @Override
    public void update(Subject s) {
        Application s1 = (Application) s;
        System.out.println(s1.getUpdate() + " " + this.getMobileName());
    }
    
}
