package mobiles;

import java.util.ArrayList;

public class Application extends Thread implements Subject  {

    private ArrayList<Observer> observers;

    private String update;
    
    private String appName;
    
    private Thread t;

    public String getAppName() {
        return appName;
    }
    
    
    public Application(String appName) {
        this.appName = appName;
        observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
        Mobile o1 = (Mobile) o;
        System.out.println(this.getAppName() + " was installed on " + o1.getMobileName());
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
        Mobile o1 = (Mobile) o;
        System.out.println(this.getAppName() + " was removed from " + o1.getMobileName());
    }

    @Override
    public void notifyObservers() {
        for (Observer o : observers) {
            o.update(this);
        }
    }

    public void setUpdate(String update) {
        this.update = update;
    }

    @Override
    public String getUpdate() {
        return update;
    }

    @Override
    public void run() {
        Observer nokia = new Mobile("Nokia Lumia");
        Observer sony = new Mobile("Sony Xperia Z");
        Observer iPhone = new Mobile("iPhone 6s");
        this.registerObserver(nokia);
        this.registerObserver(sony);
        this.registerObserver(iPhone);
        this.setUpdate(this.appName + " is updating on ");
        this.notifyObservers();
        this.removeObserver(nokia);
        this.removeObserver(sony);
        this.removeObserver(iPhone);
    }

    @Override
    public synchronized void start() {
      System.out.println("Starting of installation: " +  appName );
      if (t == null)
      {
         t = new Thread (this, appName);
         t.start ();
      }
      System.out.println(appName + " was installed");
    }
        
    
}
