package software;

public class TextArea implements GUIComponent {

    @Override
    public String getDescription() {
        return "Area for text output";
    }
}
