package software;

public abstract interface GUIComponent {

    public String getDescription();
}
