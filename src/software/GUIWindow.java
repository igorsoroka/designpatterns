package software;

public class GUIWindow {
    
    private static GUIWindow instance;

    private Button buttonOK = new Button();

    private Button buttonCancel = new Button();

    private ToolBar toolBar = new ToolBar();

    private TextArea result = new TextArea();

    private TextField searchField = new TextField();

    private Label name = new Label();

    private GUIWindow() {}
    
    public static GUIWindow getInstance() {
        if (instance == null)
            instance = new GUIWindow();
        return instance;
    }

    @Override
    public String toString() {
        return "The program graphical interface contains:\n" 
                + this.buttonOK.getDescription() + "\n" 
                + this.buttonCancel.getDescription() + "\n" +
                this.toolBar.getDescription() + "\n" + 
                this.result.getDescription() + "\n" + 
                this.searchField.getDescription() + "\n" +
                this.name.getDescription() + "\n";
    }
    
    
}
