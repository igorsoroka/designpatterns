package software;

public class TextField implements GUIComponent {

    @Override
    public String getDescription() {
        return "Text Field - Field for entering text";
    }
}
