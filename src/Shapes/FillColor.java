package shapes;

public abstract class FillColor implements Shape {

    protected Shape shape;

    public FillColor(Shape shape) {
        this.shape = shape;
    }

    public void draw() {
    }
}
