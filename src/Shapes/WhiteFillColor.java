package shapes;

public class WhiteFillColor extends FillColor {

    public WhiteFillColor(Shape shape) {
        super(shape);
    }

    @Override
    public void draw() {
        shape.draw();
        setWhiteFillColor(shape);
    }

    public void setWhiteFillColor(Shape shape) {
        System.out.println("Fill color is white");
    }
}
