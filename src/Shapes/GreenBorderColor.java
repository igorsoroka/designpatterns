package shapes;

public class GreenBorderColor extends BorderColor {

    public GreenBorderColor(Shape shape) {
        super(shape);
    }

    @Override
    public void draw() {
        shape.draw();
        setGreenBorder(shape);
    }

    public void setGreenBorder(Shape shape) {
        System.out.println("Border color is green.");
    }
}
