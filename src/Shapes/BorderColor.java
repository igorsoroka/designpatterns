package shapes;

public abstract class BorderColor implements Shape {
    protected Shape shape;
    
    public BorderColor(Shape shape) {
        this.shape = shape;
    }
    @Override
    public void draw() {
        shape.draw();
    }
}
