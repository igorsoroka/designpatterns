package shapes;

public class MainClass {

    public static void main(String[] args) {
        Shape circle = new Circle();
        System.out.println("The normal circle is created");
        circle.draw();
        Shape rectangle = new Rectangle();
        System.out.println("The normal rectangle is created");
        rectangle.draw();
        Shape line = new Line();
        System.out.println("The normal line is created");
        line.draw();
        System.out.println();
        Shape greenBorderRectangle = new GreenBorderColor(new Rectangle());
        greenBorderRectangle.draw();
        System.out.println();
        
        Shape greenBorderCircle = new GreenBorderColor(new Circle());
        greenBorderCircle.draw();
        System.out.println();
        
        Shape greenBorderLine = new GreenBorderColor(new Line());
        greenBorderLine.draw();
        System.out.println();
        
        Shape whiteFillRectangle = new WhiteFillColor(new Rectangle());
        whiteFillRectangle.draw();
        System.out.println();
        
        Shape whiteFillCircle = new WhiteFillColor(new Circle());
        whiteFillRectangle.draw();
        System.out.println();
    }
}
