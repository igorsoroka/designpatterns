package factory;

public class RussianBlue implements Cat {

    @Override
    public void meow() {
        System.out.println("Russian Blue : Meeeowwwww!");
    }

    @Override
    public void playWithKnit() {
        System.out.println("Russian Blue is playing with knit");
    }

    @Override
    public void sleep() {
        System.out.println("Russian Blue is sleeping...");
    }
}
