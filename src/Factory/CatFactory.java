package factory;

public class CatFactory implements AbstractFactory {
    private Cat cat;
    
    @Override
    public void createAnimal(String breed) {
        switch(breed) {
            case "Maine Coon":
                    cat = new MaineCoon();
            case "Russian Blue":
                    cat = new RussianBlue();
            case "Scottish Fold":
                    cat = new ScottishFold();
        }
    }

    public Cat getCat() {
        return cat;
    }
    
}
