package factory;

public interface Dog {

    public void bark();

    public void sit();

    public void run();
}
