package factory;

public class ScottishFold implements Cat {

    public void meow() {
        System.out.println("Scottishfold : Meeeowwwww!");
    }

    public void playWithKnit() {
        System.out.println("Scottishfold is playing with knit");
    }

    public void sleep() {
        System.out.println("Scottishfold is sleeping...");
    }
}
