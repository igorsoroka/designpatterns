package factory;

public interface Cat {

    public void meow();

    public void playWithKnit();

    public void sleep();
}
