package factory;

public class Dalmatian implements Dog {

    public void bark() {
        System.out.println("Dalmatian" + " is barking");
    }

    public void sit() {
        System.out.println("Dalmatian" + " is sitting");
    }

    public void run() {
        System.out.println("Dalmatian" + " is running");
    }
}
