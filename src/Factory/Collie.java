package factory;

public class Collie implements Dog {

    public void bark() {
        System.out.println("Collie" + " is barking");
    }

    public void sit() {
        System.out.println("Collie" + " is sitting");
    }

    public void run() {
        System.out.println("Collie" + " started to run");
    }
}
