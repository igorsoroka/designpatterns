package factory;

public class MaineCoon implements Cat {

    public void meow() {
        System.out.println("Maine Coon : Meeeowwwww!");
    }

    public void playWithKnit() {
        System.out.println("Maine coon is playing with knit");
    }

    public void sleep() {
        System.out.println("Maine coon is sleeping...");
    }
}
