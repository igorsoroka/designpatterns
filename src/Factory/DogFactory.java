package factory;

public class DogFactory implements AbstractFactory {
    
    private Dog dog;

    public void createAnimal(String breed) {
        switch(breed) {
            case "Dalmatian":
                    dog = new Dalmatian();
            case "English Foxhound":
                    dog = new EnglishFoxHound();
            case "Collie":
                    dog = new Collie();
        }
    }
    
    public Dog getDog() {
        return dog;
    }
}
