package factory;

public class EnglishFoxHound implements Dog {

    public void bark() {
        System.out.println("English Foxhound" + " is barking");
    }

    public void sit() {
        System.out.println("English Foxhound" + " is sitting");
    }

    public void run() {
        System.out.println("English Foxhound" + " is running");
    }
}
