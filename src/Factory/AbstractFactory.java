package factory;

public interface AbstractFactory {
    public void createAnimal(String breed);
}
