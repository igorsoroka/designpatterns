package factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainClass {

    public static void main(String[] args) throws IOException {
        ArrayList<String> menu1 = new ArrayList<>();
        menu1.add("Who do you want to create?");
        menu1.add("1 - Cat");
        menu1.add("2 - Dog");
        menu1.add("0 - Stop this!");
        
        ArrayList<String> menuCat = new ArrayList<>();
        menuCat.add("What breed do you want to choose?");
        menuCat.add("1 - Maine Coon");
        menuCat.add("2 - Russian Blue");
        menuCat.add("3 - Scottish Fold");
        
        ArrayList<String> menuCatDo = new ArrayList<>();
        menuCatDo.add("What will Cat do?");
        menuCatDo.add("1 - sleep");
        menuCatDo.add("2 - meow");
        menuCatDo.add("3 - play with knit");
        
        ArrayList<String> menuDog = new ArrayList<>();
        menuDog.add("1 - Collie");
        menuDog.add("2 - Foxhound");
        menuDog.add("3 - Dalmatian");
        
        ArrayList<String> menuDogDo = new ArrayList<>();
        menuDogDo.add("What will Dog do?");
        menuDogDo.add("1 - bark");
        menuDogDo.add("2 - run");
        menuDogDo.add("3 - sit");
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String choice1 = "";
        String choice2 = "";
        String choice3 = "";
        CatFactory cf = new CatFactory();
        DogFactory df = new DogFactory();
        while (!choice1.equals("0")) {
            menu1.stream().forEach((s) -> {
                System.out.println(s);
            });
            System.out.print("Your choice: ");
            choice1 = br.readLine();
            if (choice1.equals("1")) { 
                menuCat.stream().forEach((s) -> {
                    System.out.println(s);
                });
                System.out.print("Your choice: ");
                choice2 = br.readLine();
                if (choice2.equals("1")) {
                    cf.createAnimal("Maine Coon");
                }
                if (choice2.equals("2")) {
                    cf.createAnimal("Russian Blue");
                }
                if (choice2.equals("3")) {
                    cf.createAnimal("Scottish Fold");
                }
                System.out.println("Congratulations! " + cf.getCat().getClass().getSimpleName() + " is created!");
                
                for (String s : menuCatDo) {
                    System.out.println(s);
                }
                System.out.print("Your choice: ");
                choice3 = br.readLine();            
               
                    if (choice3.equals("1"))
                        cf.getCat().sleep();
                    if (choice3.equals("2"))
                        cf.getCat().meow();
                    if (choice3.equals("3"))
                        cf.getCat().playWithKnit();
                    
            }
            if (choice1.equals("2")) {
                menuDog.stream().forEach((s) -> {
                    System.out.println(s);
                });
                System.out.print("Your choice: ");
                choice2 = br.readLine();
                if (choice2.equals("1")) {
                    df.createAnimal("Collie");
                }
                if (choice2.equals("2")) {
                    df.createAnimal("English Foxhound");
                }
                if (choice2.equals("3")) {
                    df.createAnimal("Dalmatian");
                }
                System.out.println("Congratulations! " + df.getDog().getClass().getSimpleName() + " is created!");
                for (String s : menuDogDo) {
                    System.out.println(s);
                }
                System.out.print("Your choice: ");
                choice3 = br.readLine();            
                    if (choice3.equals("1"))
                        df.getDog().bark();
                    if (choice3.equals("2"))
                        df.getDog().run();
                    if (choice3.equals("3"))
                        df.getDog().sit();
            }
                
        }
        
    }
}
