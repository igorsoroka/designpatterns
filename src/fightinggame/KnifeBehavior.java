package fightinggame;

public class KnifeBehavior implements WeaponBehavior {

    @Override
    public void useWeapon() {
        System.out.print("cuts with a knife");
    }
}
