package fightinggame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;



public class MainClass {

    
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> menu = new ArrayList<>(); //main menu
        menu.add("1) Create character");
        menu.add("2) Fight with character");
        menu.add("0) Stop");

        ArrayList<String> subMenuChar = new ArrayList<>(); // submenu of characters
        subMenuChar.add("1) King");
        subMenuChar.add("2) Knight");
        subMenuChar.add("3) Queen");
        subMenuChar.add("4) Troll");

        ArrayList<String> subMenuWeapons = new ArrayList<>(); //submenu of weapons
        subMenuWeapons.add("1) Knife");
        subMenuWeapons.add("2) Axe");
        subMenuWeapons.add("3) Sword");
        subMenuWeapons.add("4) Club");
        int choice3 = 0;
        Character choosedChar = null;
        WeaponBehavior choosedWeapon = null;
        while(true) {
            System.out.println("*** FIGHTING SIMULATOR ***");
            menu.stream().forEach((j) -> {
                System.out.println(j);
            });
            System.out.print("Your choice: ");
            int choice = sc.nextInt();
            if (choice == 1) {
                System.out.println("Select your character: ");
                subMenuChar.stream().forEach((f) -> {
                    System.out.println(f);
                });
                System.out.print("Your choice: ");
                int choice2 = sc.nextInt();
                if (choice2 == 1) {choosedChar = new King();}
                if (choice2 == 2) {choosedChar = new Knight();}
                if (choice2 == 3) {choosedChar = new Queen();}
                if (choice2 == 4) {choosedChar = new Troll();}
                System.out.println("Choose weapon: ");
                subMenuWeapons.stream().forEach((g) -> {
                    System.out.println(g);
                });
                System.out.print("Your choice: ");
                choice3 = sc.nextInt();
                if (choice3 == 1) {choosedWeapon = new KnifeBehavior();}
                if (choice3 == 2) {choosedWeapon = new AxeBehavior();}
                if (choice3 == 3) {choosedWeapon = new SwordBehavior();}
                if (choice3 == 4) {choosedWeapon = new ClubBehavior();}
                
                    choosedChar.setWeapon(choosedWeapon);
                    System.out.println("Your chose: " + choosedChar.getClass().getSimpleName() + " with " 
                                + choosedWeapon.getClass().getSimpleName().replace("Behavior", ""));
            }
           
            if (choice == 2) {
               choosedChar.fight();
               System.out.println();
            }
            if (choice == 0) {
                break;
            }
        }
    }
}
