package fightinggame;

public interface WeaponBehavior {

    public void useWeapon();
}
