package fightinggame;

public class King extends Character {

    @Override
    public void fight() {
        System.out.print("King ");
        this.weapon.useWeapon();
    }
    
}
