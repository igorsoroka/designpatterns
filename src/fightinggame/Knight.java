package fightinggame;

public class Knight extends Character {

    @Override
    public void fight() {
        System.out.print("Knight ");
        this.weapon.useWeapon();
    }
}
